# 99Travl.

(Former Bingiz)

## Helm

```bash
helm package .
```

```bash
curl --data-binary "@ntravel-helm-1.0.0.tgz" https://charts.ssegning.com/ssegning/api/charts
```

```bash
helm dependency update ntravel-helm
```

```bash
helm dependency build ntravel-helm
```

```bash
helm install ntravel-helm ntravel-helm -n nninetravel
```

```bash
helm upgrade ntravel-helm ./ntravel-helm -n nninetravel
```

```bash
helm delete ntravel-helm -n ssegning
```
