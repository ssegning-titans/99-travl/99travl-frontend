import Document, {Head, Html, Main, NextScript} from 'next/document'

export class AppDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <link
                        href="https://storage.ssegning.com/sse.media/fonts/louis-george-cafe/99travl-font.css"
                        rel="stylesheet"
                    />
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        )
    }
}

export default AppDocument;
