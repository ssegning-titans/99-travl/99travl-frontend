import {AppNavBar} from "@components/navbar";
import {Container} from "react-bootstrap";
import {Header} from "@components/header";
import {AppFooter} from "@components/footer";
import React from "react";
import {PrivacyPolicyGerman} from "@privacy-policy/de";
import {PrivacyPolicyEnglish} from "@privacy-policy/en";
import {PrivacyPolicyFrench} from "@privacy-policy/fr";
import {useRouter} from "next/router";
import {GetServerSideProps, GetStaticPropsContext} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

function PrivacyPolicy() {
    const router = useRouter();

    const Child = router.locale === 'de'
        ? PrivacyPolicyGerman
        : router.locale === 'fr'
            ? PrivacyPolicyFrench
            : PrivacyPolicyEnglish;

    return (
        <div>
            <Header title='privacy-policy'/>

            <AppNavBar active='resources'/>

            <Container className='text-justify'>
                <Child/>
            </Container>

            <AppFooter/>
        </div>
    );
}

export default PrivacyPolicy;


export const getServerSideProps: GetServerSideProps = async ({locale}: GetStaticPropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'footer', 'welcome', 'navbar', 'header']),
        }
    });
};
