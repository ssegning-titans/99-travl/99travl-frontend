import {Header} from "@components/header";
import {AppNavBar} from "@components/navbar";
import {AppFooter} from "@components/footer";
import {Container} from "react-bootstrap";
import {GetServerSideProps, GetServerSidePropsContext} from "next";
import React from "react";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

interface Props {
    formationId: string;
}

function SupportFormation({formationId}: Props) {
    return (
        <div>
            <Header title={formationId}/>

            <AppNavBar/>

            <Container>
                {'Support formation ' + formationId}
            </Container>

            <AppFooter/>
        </div>
    );
}

export default SupportFormation;

export const getServerSideProps: GetServerSideProps = async ({locale, query}: GetServerSidePropsContext) => {
    const {id: formationId} = query;
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'support', 'footer', 'welcome', 'about', 'navbar']),
            formationId
        }
    });
};
