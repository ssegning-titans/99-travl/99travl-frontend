import {AppNavBar} from "@components/navbar";
import {Button, Card, Col, Container, Row} from "react-bootstrap";
import {Header} from "@components/header";
import {AppFooter} from "@components/footer";
import {GetServerSideProps, GetServerSidePropsContext} from "next";
import {I18n, useTranslation} from "next-i18next";
import React from "react";
import {Cont} from "@components/snippets";
import {FAQPageJsonLd} from 'next-seo';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

import Link from 'next/link';

interface Props extends I18n {
    children?: any;
    arr: any[];
}

export const getServerSideProps: GetServerSideProps = async ({req, locale}: GetServerSidePropsContext) => {
    const arr = [
        {
            title: 'Create an account',
            description: 'How to create an application',
            smallResponse: 'First download the app and log in using either your phone number or your email',
            id: 'create-account'
        }
    ];
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'support', 'footer', 'welcome', 'about', 'navbar']),
            arr
        }
    });
};

function SupportItem({title, description, actionPath}) {
    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    {title}
                </Card.Title>
                <Card.Text>
                    {description}
                </Card.Text>

                <Link href={'/resources/support' + actionPath} passHref>
                    <Button variant="primary">
                        See
                    </Button>
                </Link>
            </Card.Body>
        </Card>
    )
}

const Support: React.FC = ({arr}: Props) => {
    const {t} = useTranslation('support');

    return (
        <>
            <FAQPageJsonLd
                mainEntity={arr.map(i => ({
                    acceptedAnswerText: i.smallResponse,
                    questionName: i.title,
                }))}
            />

            <Header title='support'/>

            <AppNavBar active='resources'/>

            <Cont gradBg center>
                <h1 className='h1 font-weight-light'>
                    {t('support:title')}
                </h1>
                <p className='text-white-50'>
                    {t('support:description')}
                </p>
            </Cont>

            <Container>
                <Row>
                    <Col xs={12} sm={6} md={4} lg={3} className='p-2'>
                        {arr.map((item) => (
                            <SupportItem
                                key={item.id}
                                title={item.title}
                                description={item.description}
                                actionPath={'/' + item.id}
                            />
                        ))}
                    </Col>
                </Row>
            </Container>

            <AppFooter/>
        </>
    );
}

export default Support;
