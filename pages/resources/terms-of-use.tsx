import {AppNavBar} from "@components/navbar";
import {Container} from "react-bootstrap";
import {Header} from "@components/header";
import {AppFooter} from "@components/footer";
import {GetServerSideProps, GetStaticPropsContext} from "next";
import {TermsOfUseEnglish} from "@terms-of-use/en";
import {TermsOfUseGerman} from "@terms-of-use/de";
import {TermsOfUseFrench} from "@terms-of-use/fr";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {useRouter} from "next/router";

export default function TermsOfUse() {
    const router = useRouter();

    const Child = router.locale === 'de'
        ? TermsOfUseGerman
        : router.locale === 'fr'
            ? TermsOfUseFrench
            : TermsOfUseEnglish;

    return (
        <div>
            <Header title='terms-of-use'/>

            <AppNavBar active='resources'/>

            <Container className='text-justify'>
                <Child/>
            </Container>

            <AppFooter/>
        </div>
    );
}


export const getServerSideProps: GetServerSideProps = async ({locale}: GetStaticPropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'footer', 'welcome', 'navbar', 'header']),
        }
    });
};
