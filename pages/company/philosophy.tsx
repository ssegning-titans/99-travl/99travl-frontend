import {Header} from "@components/header";
import {AppNavBar} from "@components/navbar";
import {Container} from "react-bootstrap";
import {AppFooter} from "@components/footer";
import {GetServerSideProps, GetStaticPropsContext} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

export default function Philosophy() {
    return (
        <div>
            <Header title='philosophy'/>

            <AppNavBar active='company'/>

            <Container>
                Philosophy
            </Container>

            <AppFooter/>
        </div>
    );
}

export const getServerSideProps: GetServerSideProps = async ({locale}: GetStaticPropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'footer', 'contact', 'welcome', 'navbar', 'header']),
        }
    });
};
