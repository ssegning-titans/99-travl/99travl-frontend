import {Header} from "@components/header";
import {AppNavBar} from "@components/navbar";
import {AppFooter} from "@components/footer";
import {GetServerSideProps, GetStaticPropsContext} from "next";
import {Cont} from "@components/snippets";
import React from "react";
import {Button, Carousel, Col, Image, Row} from "react-bootstrap";
import {useTranslation} from "next-i18next";
import Link from 'next/link';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

interface CarIteProps {
    title: string;
    post: string;
    description: string;
    img: string;
}

const list: CarIteProps[] = [
    {
        title: 'Segning Lambou, S.',
        post: 'CTO, Dev Team',
        description: 'Dev Advocate, Always thinking of new ideas, new ways to develop, improve our project.',
        img: '/images/selast.jpg'
    },
];

function About() {
    const {t} = useTranslation('about');
    return (
        <div>
            <Header title='about'/>

            <AppNavBar active='company'/>

            <Cont center>
                <h1 className='h1 font-weight-light'>
                    {t('title')}
                </h1>
                <p className='px-md-5 mx-md-5'>
                    {t('description')}
                </p>
            </Cont>

            <Cont className='d-none d-sm-block' src='/images/team.jpg' center>
                <div style={{height: '10rem'}}/>
            </Cont>

            <Cont>
                <Row>

                    <Col className='text-center' xs={12} md={4}>
                        <Image alt='idea image' src='/images/idea.png' className='d-none d-md-inline w-25 pb-3'/>
                        <h3>{t('common:creativity')}</h3>
                        <p>{t('creativity')}</p>
                    </Col>

                    <Col className='text-center' xs={12} md={4}>
                        <Image alt='world image' src='/images/planet-earth.png'
                               className='d-none d-md-inline w-25 pb-3'/>
                        <h3>{t('common:worldwide')}</h3>
                        <p>{t('worldwide')}</p>
                    </Col>

                    <Col className='text-center' xs={12} md={4}>
                        <Image alt='world image' src='/images/online-shop.png'
                               className='d-none d-md-inline w-25 pb-3'/>
                        <h3>{t('common:unique-approach')}</h3>
                        <p>{t('unique-approach')}</p>
                    </Col>

                </Row>
            </Cont>

            <Cont>
                <Carousel
                    indicators={false}
                    nextIcon={(
                        <Image alt='next image' src='/images/next.png' width={24} height={24}/>
                    )}
                    prevIcon={(
                        <Image alt='prev image' src='/images/prev.png' width={24} height={24}/>
                    )}
                >
                    {list.map(({title, post, description, img}: CarIteProps) => (
                        <Carousel.Item key={title}>
                            <Row className='justify-content-md-center align-items-md-center'>
                                <Col md={4} className="text-center text-md-right">
                                    <Image
                                        src={img}
                                        alt={title}
                                        roundedCircle
                                        width={200}
                                        height={200}
                                        className="d-none d-md-inline-block"
                                    />

                                    <Image
                                        src={img}
                                        alt={title}
                                        roundedCircle
                                        width={100}
                                        height={100}
                                        className="d-inline-block d-md-none"
                                    />
                                </Col>

                                <Col md={4}>
                                    <p className='text-dark font-weight-light font-italic'>{description}</p>
                                    <h3 className='text-dark'>{title}</h3>
                                    <h6 className='text-dark font-weight-light'>{post}</h6>
                                </Col>
                            </Row>

                        </Carousel.Item>
                    ))}
                </Carousel>
            </Cont>

            <Cont center gradBg>
                <h1 className='h1 font-weight-light'>{t('contact')}</h1>

                <p>
                    <Link href='/company/contact' passHref>
                        <Button variant='light'>Continue on Contact page</Button>
                    </Link>
                </p>
            </Cont>

            <AppFooter/>
        </div>
    );
}

export default About;

export const getServerSideProps: GetServerSideProps = async ({locale}: GetStaticPropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'footer', 'welcome', 'about', 'navbar', 'header']),
        }
    });
};
