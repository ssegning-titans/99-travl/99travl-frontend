import {Header} from "@components/header";
import {AppNavBar} from "@components/navbar";
import {Button, Col, Container, Row} from "react-bootstrap";
import {AppFooter} from "@components/footer";
import {GetServerSideProps, GetServerSidePropsContext} from "next";
import React, {memo} from "react";
import GoogleMapReact from 'google-map-react';
import {Cont} from "@components/snippets";
import ContactForm from '@contact/contact-form';
import {CorporateContactJsonLd} from 'next-seo';
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

export const getServerSideProps: GetServerSideProps = async ({req, locale}: GetServerSidePropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'footer', 'contact', 'welcome', 'navbar', 'header']),
            googleApiKey: process.env.GOOGLE_MAPS_API
        }
    });
};

const HelloMarker: (a: any) => JSX.Element = ({text}) => <p>{text}</p>;

const Contact = memo(({baseUrl, googleApiKey}: any) => {
    return (
        <>
            <CorporateContactJsonLd
                url={baseUrl}
                logo={baseUrl + '/logo.png'}
                contactPoint={[
                    {
                        telephone: '+4917668488361',
                        contactType: 'Customer service',
                        areaServed: 'DE',
                        availableLanguage: ['English', 'German', 'French', 'Arabic'],
                    },
                ]}
            />

            <Header title='contact'/>

            <AppNavBar active='company'/>

            <Container>
                <Cont center>
                    <h1>Contact</h1>
                </Cont>
            </Container>

            <Container fluid>
                <Row className='align-items-md-center rounded'>
                    <Col xs={12} md={8} style={{height: '50vh', width: '100%'}}>
                        <GoogleMapReact defaultZoom={12}
                                        defaultCenter={{
                                            lat: 3.8480,
                                            lng: 11.5021
                                        }}
                                        bootstrapURLKeys={{key: googleApiKey}}>
                            <HelloMarker
                                lat={3.8480}
                                lng={11.5021}
                                text="Hello!"
                            />
                        </GoogleMapReact>
                    </Col>

                    <Col className='py-5'>
                        <h1 className='h1 font-weight-light'>
                            Location
                        </h1>
                        <p>
                            We are actually in Nuremberg, Germany. We then have more and more choices for any action

                            There is also our Company registered.
                        </p>
                    </Col>
                </Row>
            </Container>

            <Cont gradBg center>
                <Row className='justify-content-md-center'>
                    <Col xs={12} md={10} lg={8} xl={7}>
                        <h1 className='h1 font-weight-light'>
                            Contact us
                        </h1>
                        <p className='text-white-50'>
                            Whenever you need an information or more. Feel free to email us at
                        </p>

                        <Button variant='light' href='mailto:hello@99travl.com' title='Send 99travl an Email'>
                            hello@99travl.com
                        </Button>
                    </Col>
                </Row>
            </Cont>

            <Cont center>
                <Row className='justify-content-md-center'>
                    <Col xs={12} md={10} lg={8} xl={7}>
                        <h3 className='h3 font-weight-light'>
                            Or use our custom form for this purpose.
                        </h3>

                        <ContactForm/>
                    </Col>
                </Row>
            </Cont>

            <AppFooter/>
        </>
    );
});

export default Contact;
