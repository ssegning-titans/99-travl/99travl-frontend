import '../styles/globals.scss'
import {appWithTranslation} from 'next-i18next';
import nextI18NextConfig from '../next-i18next.config.js'
import SeoConfig from "@common/seo";
import {DefaultSeo} from "next-seo";
import absoluteUrl from "next-absolute-url";
import App, {AppContext, AppInitialProps} from "next/app";

function MyApp({Component, pageProps}) {
    const {baseUrl, locale} = pageProps;
    const seoProps = SeoConfig({baseUrl, locale});
    return (
        <>
            <DefaultSeo {...seoProps}/>
            <Component {...pageProps} />
        </>
    );
}

MyApp.getInitialProps = async (appContext: AppContext) => {
    const props: AppInitialProps = await App.getInitialProps(appContext);
    const {origin} = absoluteUrl(appContext.ctx.req);

    return ({
        ...props,
        pageProps: {
            ...props.pageProps,
            baseUrl: origin,
            locale: appContext.router.locale
        },
    });
};

export default appWithTranslation(MyApp, nextI18NextConfig);
