import {AppFooter} from "@components/footer";
import {Col, Container, Image, Media, Row} from "react-bootstrap";
import {memo} from "react";
import {AppNavBar} from "@components/navbar";
import Link from 'next/link';
import {useTranslation} from "next-i18next";
import {Header} from "@components/header";
import {GetServerSideProps, GetStaticPropsContext} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";


const MediaItem = memo(({title, description, key}: any) => (
    <Media className='col-md-6 mb-2 mb-md-4'>
        <Image
            alt={'avatar image ' + key}
            width={72}
            height={72}
            src="https://via.placeholder.com/150"
            className="mr-3"
            roundedCircle
        />
        <Media.Body>
            <h5>{title}</h5>
            <p>{description}</p>
        </Media.Body>
    </Media>
));

function Home() {
    const {t: wt} = useTranslation('welcome');
    return (
        <div>
            <Header title='home'/>

            <AppNavBar active='home'/>

            <div className='bg-lighter py-4 py-lg-5'>
                <Container id='home'>
                    <Row className='align-items-md-center justify-content-md-between'>
                        <Col md={4}>
                            <h1 className='h1 font-weight-light title-4 py-3 py-md-5'>
                                {wt('catchPhrase')}
                            </h1>
                            <p className='text-secondary'>
                                {wt('catchPhrase')}
                            </p>

                            <Row className='justify-content-start pb-3 pb-md-4'>
                                <Col xs={12} md='auto'>
                                    <a rel='noopener' target='_blank' href='https://play.google.com/store/apps/details?id=com.ssegning.ntravl'>
                                        <Image alt='play image' height={52} src="/stores/google-play-badge.png"
                                               className='my-1'/>
                                    </a>
                                </Col>
                                <Col xs={12} md='auto'>
                                    <a href='/'>
                                        <Image alt='app image' height={52} src="/stores/appstore.png" className='my-1'/>
                                    </a>
                                </Col>
                            </Row>
                        </Col>

                        <Col md={5} className='d-none d-md-block text-center px-5'>
                            <Image
                                src="https://colorlib.com/preview/theme/appco/assets/img/hero/hero_right.png"
                                className='w-100'
                                alt="Open shop"
                            />
                        </Col>
                    </Row>
                </Container>
            </div>

            <Container id='product' className='py-4 py-lg-5'>
                <Row className='justify-content-md-between align-items-md-center'>
                    <Col md={4} className='d-none d-md-block'>
                        <Image
                            src='https://colorlib.com/preview/theme/appco/assets/img/shape/best-features.png'
                            className='w-100'
                            alt='avatar'
                        />
                    </Col>
                    <Col md={7}>
                        <h1 className='h1 font-weight-light mb-md-4'>
                            La Logistique 2.0
                        </h1>

                        <Row>
                            <MediaItem
                                title='Media Heading'
                                description='Cras sit amet nibh libero, in gravida nulla.'
                            />

                            <MediaItem
                                title='Media Heading'
                                description='Cras sit amet nibh libero, in gravida nulla.'
                            />

                            <MediaItem
                                title='Media Heading'
                                description='Cras sit amet nibh libero, in gravida nulla.'
                            />

                            <MediaItem
                                title='Media Heading'
                                description='Cras sit amet nibh libero, in gravida nulla.'
                            />
                        </Row>
                    </Col>
                </Row>
            </Container>

            <AppFooter/>
        </div>
    )
}

export default Home;

export const getServerSideProps: GetServerSideProps = async ({locale}: GetStaticPropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['common', 'footer', 'welcome', 'navbar', 'header']),
        }
    });
};
