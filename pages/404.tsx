import React from "react";
import {InternalServerError} from "@components/internal-server-error";
import {GetStaticProps, GetStaticPropsContext} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

function Error() {
    return (
        <InternalServerError statusCode={404}/>
    )
}

export default Error;

export const getStaticProps: GetStaticProps = async ({locale}: GetStaticPropsContext) => {
    return ({
        props: {
            ...await serverSideTranslations(locale, ['internal-error', 'common', 'footer', 'welcome', 'navbar', 'header']),
        }
    });
};
