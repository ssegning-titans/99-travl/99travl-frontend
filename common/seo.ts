import {DefaultSeoProps} from "next-seo";

interface BaseInfo {
    baseUrl: string;
    locale: string;
}

const SeoConfig: (props: BaseInfo) => DefaultSeoProps = ({baseUrl, locale}) => ({
    openGraph: {
        type: 'website',
        locale: locale,
        url: baseUrl,
        site_name: '99Travl',
        title: '99Travl - All travels by you!',
        description: 'La Logistique réinventée à moindre coût',
        images: [
            {
                url: baseUrl + '/images/open-shop.jpg',
                width: 822,
                height: 640,
                alt: '99Travl, Worldwide',
            },
            {
                url: baseUrl + '/images/team.jpg',
                width: 1080,
                height: 365,
                alt: '99Travl, Team',
            },
        ],
    },
    twitter: {
        site: '@99travl',
        handle: '@99travl',
        cardType: 'summary_large_image',
    },
    languageAlternates: [
        {
            hrefLang: 'de',
            href: 'https://de.99travl.com',
        },
        {
            hrefLang: 'en',
            href: 'https://99travl.com',
        },
        {
            hrefLang: 'fr',
            href: 'https://fr.99travl.com',
        },
        {
            hrefLang: 'ar',
            href: 'https://ar.99travl.com',
        },
        {
            hrefLang: 'x-default',
            href: 'https://99travl.com',
        },
    ].filter(m => m.hrefLang !== locale),
    additionalMetaTags: [
        {
            property: 'dc:creator',
            content: 'S. Segning <https://ssegning.com>'
        },
        {
            name: 'application-name',
            content: '99travl'
        }
    ],
    facebook: {
        appId: '2835800300027208'
    },
    canonical: baseUrl
});

export default SeoConfig;
