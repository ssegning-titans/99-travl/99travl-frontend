export function isSafari(ua: string) {
    if (!ua) return false;
    const w = ua.toLowerCase();
    if (w.indexOf('safari') != -1) {
        return w.indexOf('chrome') <= -1;
    }
    return false;
}
