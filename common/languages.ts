export const languageLabels = [
    {label: 'Français', value: 'fr'},
    {label: 'English', value: 'en'},
    {label: 'Deutsch', value: 'de'},
    {
        label: 'عرب',
        value: 'ar'
    },
];
