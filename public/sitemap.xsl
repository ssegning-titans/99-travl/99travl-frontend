<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="2.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"
        xmlns:xhtml="http://www.w3.org/1999/xhtml">

    <xsl:output method="html" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>
                    99travl Sitemap
                    <xsl:if test="sitemap:sitemapindex">Index</xsl:if>
                </title>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
            </head>
            <body>
                <div class="bg-primary py-2 mb-3 text-white"
                     style="background: var(--primary) url(https://storage.ssegning.com/99travl/logo.png) no-repeat right">
                    <header class="container">
                        <div class="w-100 mb-3">
                            <h1 class="display-4">99Travl. Sitemap</h1>
                            <xsl:if test="sitemap:sitemapindex">
                                <span class="badge badge-pill badge-light p-2 px-3 mx-1">Index</span>
                            </xsl:if>
                            <xsl:if test="sitemap:urlset/sitemap:url/image:image">
                                <span class="badge badge-pill badge-light p-2 px-3 mx-1">Images</span>
                            </xsl:if>
                            <xsl:if test="sitemap:urlset/sitemap:url/video:video">
                                <span class="badge badge-pill badge-light p-2 px-3 mx-1">Video</span>
                            </xsl:if>
                            <xsl:if test="sitemap:urlset/sitemap:url/xhtml:link">
                                <span class="badge badge-pill badge-light p-2 px-3 mx-1">Xhtml</span>
                            </xsl:if>
                            <xsl:if test="sitemap:sitemapindex/sitemap:sitemap/xhtml:link">
                                <span class="badge badge-pill badge-light p-2 px-3 mx-1">Xhtml</span>
                            </xsl:if>
                        </div>
                        <h2 class="font-weight-light">
                            <xsl:choose>
                                <xsl:when test="sitemap:sitemapindex">
                                    This index contains
                                    <strong class="blue">
                                        <xsl:value-of select="count(sitemap:sitemapindex/sitemap:sitemap)"/>
                                    </strong>
                                    sitemaps.
                                </xsl:when>
                                <xsl:otherwise>
                                    This index contains
                                    <strong class="blue">
                                        <xsl:value-of select="count(sitemap:urlset/sitemap:url)"/>
                                    </strong>
                                    URLs.
                                </xsl:otherwise>
                            </xsl:choose>
                        </h2>
                        <p class="text-white-50">
                            This is an XML sitemap, meant for consumption by search engines.
                            <br/>
                            You can find more information about XML sitemaps on <a href="http://www.sitemaps.org"
                                                                                   target="_blank"
                                                                                   rel='noopener'
                                                                                   class="text-decoration-none text-white">
                            sitemaps.org</a>.
                        </p>
                    </header>
                </div>

                <div class="container">
                    <xsl:apply-templates/>

                    <footer class="mw8 center pv4 tc">
                        This is an open source XML Sitemap Stylesheet created by
                        <a href="https://99travl.com"
                           title="99travl."
                           target="_blank"
                           rel='noopener'
                           class="link blue">99travl.com
                        </a>
                    </footer>
                </div>

            </body>
        </html>
    </xsl:template>

    <xsl:template match="sitemap:sitemapindex">
        <div class="overflow-auto">
            <table class="w-100 table table-striped table-bordered table-hover">
                <thead class="bg-silver">
                    <tr>
                        <th></th>
                        <th>URL</th>
                        <xsl:if test="sitemap:sitemap/sitemap:priority">
                            <th style="width:90px">Priority</th>
                        </xsl:if>
                        <xsl:if test="sitemap:sitemap/sitemap:lastmod">
                            <th style="width:200px">Last Modified</th>
                        </xsl:if>
                    </tr>
                </thead>
                <tbody class="lh-copy bg-near-white">
                    <xsl:for-each select="sitemap:sitemap">
                        <tr class="hover-bg-white">
                            <xsl:variable name="loc">
                                <xsl:value-of select="sitemap:loc"/>
                            </xsl:variable>
                            <xsl:variable name="pno">
                                <xsl:value-of select="position()"/>
                            </xsl:variable>
                            <td>
                                <xsl:value-of select="$pno"/>
                            </td>
                            <td>
                                <h5 class="pb-4">
                                    <span class="badge badge-primary mr-3">Loc</span>
                                    <a href="{$loc}"
                                       target="_blank"
                                       rel='noopener'
                                       class="text-decoration-none">
                                        <xsl:value-of select="sitemap:loc"/>
                                    </a>
                                </h5>
                                <xsl:apply-templates select="xhtml:*"/>
                            </td>
                            <xsl:if test="sitemap:lastmod">
                                <td>
                                    <xsl:value-of
                                            select="concat(substring(sitemap:lastmod, 0, 11), concat(' ', substring(sitemap:lastmod, 12, 5)), concat(' ', substring(sitemap:lastmod, 20, 6)))"/>
                                </td>
                            </xsl:if>
                            <xsl:apply-templates select="sitemap:changefreq|sitemap:priority"/>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </div>
    </xsl:template>

    <xsl:template match="sitemap:urlset">
        <table cellspacing="0" class="w-100 table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th>URL</th>
                    <xsl:if test="sitemap:url/sitemap:changefreq">
                        <th style="width:130px">Change Freq.</th>
                    </xsl:if>
                    <xsl:if test="sitemap:url/sitemap:priority">
                        <th style="width:90px">Priority</th>
                    </xsl:if>
                    <xsl:if test="sitemap:url/sitemap:lastmod">
                        <th style="width:200px">Last Modified</th>
                    </xsl:if>
                </tr>
            </thead>
            <tbody>
                <xsl:for-each select="sitemap:url">
                    <tr>
                        <xsl:variable name="loc">
                            <xsl:value-of select="sitemap:loc"/>
                        </xsl:variable>
                        <xsl:variable name="pno">
                            <xsl:value-of select="position()"/>
                        </xsl:variable>
                        <td>
                            <xsl:value-of select="$pno"/>
                        </td>
                        <td>
                            <p>
                                <span class="badge badge-primary d-inline-block mr-3">Loc</span>
                                <a href="{$loc}"
                                   target="_blank"
                                   rel='noopener'
                                   class="text-decoration-none">
                                    <xsl:value-of select="sitemap:loc"/>
                                </a>
                            </p>
                            <xsl:apply-templates select="image:*"/>
                            <xsl:apply-templates select="video:*"/>
                            <xsl:apply-templates select="xhtml:*"/>
                        </td>
                        <xsl:apply-templates select="sitemap:changefreq"/>
                        <xsl:apply-templates select="sitemap:priority"/>
                        <xsl:if test="sitemap:lastmod">
                            <td class="font-bold">
                                <xsl:value-of
                                        select="concat(substring(sitemap:lastmod, 0, 11), concat(' ', substring(sitemap:lastmod, 12, 5)), concat(' ', substring(sitemap:lastmod, 20, 6)))"/>
                            </td>
                        </xsl:if>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="sitemap:loc|sitemap:lastmod|image:loc|image:caption|video:*|xhtml:*">
        <td class="font-bold">
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="sitemap:changefreq|sitemap:priority">
        <td class="font-bold">
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="xhtml:link">
        <xsl:variable name="altloc">
            <xsl:value-of select="@href"/>
        </xsl:variable>

        <li class="media mb-3">
            <div class="media-body">
                <h6 class="mt-0 mb-1">
                    Xhtml:
                    <a href="{$altloc}"
                       target="_blank"
                       rel='noopener'
                       class="text-decoration-none pr-2">
                        <xsl:value-of select="@href"/>
                    </a>

                    <xsl:if test="@hreflang">
                        <small class="mr-2 badge badge-primary">
                            <xsl:value-of select="@hreflang"/>
                        </small>
                    </xsl:if>

                    <xsl:if test="@rel">
                        <small class="mr-2 badge badge-info">
                            <xsl:value-of select="@rel"/>
                        </small>
                    </xsl:if>

                    <xsl:if test="@media">
                        <small>
                            <xsl:value-of select="@media"/>
                        </small>
                    </xsl:if>
                </h6>
            </div>
        </li>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="image:image">
        <xsl:variable name="loc">
            <xsl:value-of select="image:loc"/>
        </xsl:variable>
        <xsl:variable name="lic">
            <xsl:value-of select="image:license"/>
        </xsl:variable>

        <div class="media">
            <img src="{$loc}?width=512&amp;height=512" style="height: 50px; width: 50px"
                 class="align-self-start mr-3 rounded-circle border border-success"
                 alt="img"/>
            <div class="media-body">
                <h5 class="mt-0">
                    <xsl:if test="image:caption">
                        <span>
                            <xsl:value-of select="image:caption"/>
                        </span>
                    </xsl:if>
                </h5>
                <p>
                    <a href="{$loc}"
                       target="_blank"
                       rel='noopener'
                       class="text-decoration-none">
                        <xsl:value-of select="image:loc"/>
                    </a>
                </p>
                <p>
                    This image is under the license
                    <a href="{$lic}"
                       target="_blank"
                       rel='noopener'
                       class="text-decoration-none">
                        <xsl:value-of select="image:license"/>
                    </a>
                </p>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="video:video">
        <xsl:variable name="loc">
            <xsl:choose>
                <xsl:when test="video:player_loc != ''">
                    <xsl:value-of select="video:player_loc"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="video:content_loc"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="thumb_loc">
            <xsl:value-of select="video:thumbnail_loc"/>
        </xsl:variable>
        <p>
            <strong>Video:</strong>
            <a href="{$loc}"
               rel='noopener'
               target="_blank"
               class="mr2 link blue">
                <xsl:choose>
                    <xsl:when test="video:player_loc != ''">
                        <xsl:value-of select="video:player_loc"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="video:content_loc"/>
                    </xsl:otherwise>
                </xsl:choose>
            </a>
            <a href="{$thumb_loc}"
               rel='noopener'
               target="_blank"
               class="dib mr2 ph2 pv1 tracked lh-solid link white bg-silver hover-bg-blue br-pill">
                thumb
            </a>
            <xsl:if test="video:title">
                <span class="i gray">
                    <xsl:value-of select="video:title"/>
                </span>
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

</xsl:stylesheet>
