import Head from "next/head";
import {useTranslation} from "next-i18next";
import {memo} from "react";
import {useRouter} from "next/router";


interface HeaderProps {
    title: string;
}

export const Header = memo(({title}: HeaderProps) => {
    const {t} = useTranslation('header');
    const router = useRouter();
    return (
        <Head>
            <title>{title ? t(title) + ' - ' : ''}99Travl</title>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
            <link rel="icon" href="/favicon.ico"/>
            <link rel="manifest" href={'/manifest-' + router.locale + '.json'}/>
            <link rel="mask-icon" href="/logo.png" color="#ff00ff"/>
            <meta name="msapplication-TileColor" content="#ff00ff"/>
            <meta name="theme-color" content="#eeeeee"/>
        </Head>
    );
});
