import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {useCallback, useMemo} from "react";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";
import Link from "next/link";
import {languageLabels} from "@common/languages";

interface AppNavBarProps {
    active?: 'about' | 'home' | 'resources' | 'company' | 'announces';
}

export function AppNavBar({active}: AppNavBarProps) {

    const {t} = useTranslation('navbar');
    const router = useRouter();

    const lang = useMemo(() => router.locale, [router?.locale]);

    const handleLanguageChange = useCallback((lang: string) => () => {
        if (!router) return;
        const route = router.route;
        router.push(route, route, {locale: lang})
    }, [router?.locale]);

    return (
        <Navbar collapseOnSelect expand="md" variant="light" sticky="top"
                className='mb-md-5 font-title font-weight-light' bg='white'>
            <Container>
                <Link passHref href="/">
                    <Navbar.Brand>
                        <img
                            src="/logo.png"
                            height="42"
                            width='109.16'
                            className="d-inline-block align-top"
                            alt="99Travl logo"
                        />
                    </Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                        <Link passHref href="/#home">
                            <Nav.Link active={active === 'home'}>
                                {t('home')}
                            </Nav.Link>
                        </Link>

                        <Link passHref href="/#product">
                            <Nav.Link>
                                {t('product')}
                            </Nav.Link>
                        </Link>

                        <Link passHref href="/company/about">
                            <Nav.Link active={active === 'company'}>
                                {t('company')}
                            </Nav.Link>
                        </Link>

                        <Link passHref href="/resources/support">
                            <Nav.Link active={active === 'resources'}>
                                {t('resources')}
                            </Nav.Link>
                        </Link>

                        <NavDropdown title={lang.toLocaleUpperCase()} id="language-nav-dropdown">
                            {languageLabels.map((language, index) => (
                                <NavDropdown.Item key={language.value} onSelect={handleLanguageChange(language.value)}>
                                    {language.label}
                                </NavDropdown.Item>
                            ))}
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

AppNavBar.getInitialProps = async () => ({
    namespacesRequired: ['navbar'],
});
