import * as React from "react";
import {Header} from "@components/header";
import {AppNavBar} from "@components/navbar";
import {Button, Col, Container, Row} from "react-bootstrap";
import {AppFooter} from "@components/footer";
import {useTranslation} from "next-i18next";
import {NextSeo} from "next-seo";
import Link from 'next/link';

interface Props {
    statusCode: number;
    message?: string;
}

export function InternalServerError({statusCode, message}: Props) {
    const isNotFound = statusCode === 404;
    const {t} = useTranslation('internal-error');
    const errorKey = message ? message : isNotFound ? 'page-not-found' : 'internal-server-error';
    return (
        <div>
            <NextSeo nofollow/>
            <Header title='internal-server-error'/>

            <AppNavBar/>

            <Container>

                <Row>

                    <Col/>

                    <Col md={5} xs={12} className='text-left text-md-center py-3 py-md-5'>
                        <h1 className='h1 font-weight-light display-1 text-primary font-weight-bold'>{isNotFound ? 404 : 500}</h1>
                        <h2>Oops!</h2>
                        <h3>
                            {t('error.' + errorKey)}
                        </h3>
                        <p className='text-secondary mb-3 mb-md-5'>
                            {t('description.' + errorKey)}
                        </p>

                        <Link href='/' passHref>
                            <Button as='a'>{t('common:back-to-homepage')}</Button>
                        </Link>
                    </Col>

                    <Col/>

                </Row>

            </Container>

            <AppFooter/>
        </div>
    )
}
