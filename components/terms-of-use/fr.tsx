import React, {memo} from "react";

export const TermsOfUseFrench = memo(() => (
    <>
        <h1 className='text-primary'>
            Conditions générales
        </h1>

        <p>Bienvenue à 99 Travel!</p>

        <p>
            Cette page existe aussi en <a href='/en/resources/terms-of-use'>anglais</a>.
            En cas de conflits, la version anglaise prévaut.
        </p>

        <p>Les présentes conditions générales décrivent les règles et les réglementations relatives à l'utilisation du
            site web de 99travl,
            à l'adresse suivante : https://99travl.com.</p>

        <p>En accédant à ce site web, nous supposons que vous acceptez ces conditions générales. Ne continuez pas à
            utiliser
            99travl si vous n'acceptez pas tous les termes et conditions énoncés sur cette page.</p>

        <p>La terminologie suivante s'applique aux présentes conditions générales, à la déclaration de confidentialité
            et à la clause de non-responsabilité
            Avis et tous les accords : "Client", "Vous" et "Votre" se réfèrent à vous, la personne qui se connecte sur
            ce site
            et conforme aux conditions de la société. "La société", "Nous", "Notre", "Nos" et
            "Nous", fait référence à notre société. "Partie", "Parties", ou "Nous", désigne à la fois le Client et
            nous-mêmes.
            Tous les termes se réfèrent à l'offre, l'acceptation et la considération du paiement nécessaire pour
            entreprendre le
            de notre assistance au client de la manière la plus appropriée dans le but exprès
            répondre aux besoins du client en ce qui concerne la fourniture des services indiqués par la société,
            conformément
            avec et sous réserve de la législation en vigueur aux Pays-Bas. Toute utilisation de la terminologie
            ci-dessus ou d'autres mots
            au singulier, au pluriel, en majuscules et/ou il/elle ou ils, sont considérés comme interchangeables et
            donc comme se référant à la même chose. Nos conditions générales ont été créées à l'aide de l'outil <a
                href="https://www.privacypolicyonline.com/terms-conditions-generator/">Conditions générales
                Generator</a> et le <a href="https://www.generateprivacypolicy.com">Politique de confidentialité
                Générateur</a>.
        </p>

        <h2 className='text-primary'>Cookies</h2>

        <p>Nous utilisons des cookies. En accédant à 99travl, vous avez accepté d'utiliser des cookies en accord avec
            la
            Politique de confidentialité de 99travl.</p>

        <p>La plupart des sites web interactifs utilisent des cookies pour nous permettre de retrouver les coordonnées
            de l'utilisateur à chaque visite. Cookies
            sont utilisés par notre site web pour permettre la fonctionnalité de certains domaines afin de faciliter la
            tâche des personnes
            en visitant notre site web. Certains de nos partenaires publicitaires/affiliés peuvent également utiliser
            des cookies.</p>

        <h2 className='text-primary'>License</h2>

        <p> Sauf indication contraire, 99travl et/ou ses concédants de licence détiennent les droits de propriété
            intellectuelle pour tous
            sur 99travl. Tous les droits de propriété intellectuelle sont réservés. Vous pouvez y accéder à partir de
            99travl pour votre usage personnel soumis aux restrictions définies dans les présentes conditions
            générales.</p>

        <p>Vous ne devez pas:</p>
        <ul>
            <li>Republier le matériel de 99travl</li>
            <li>Vendre, louer ou sous-licencier du matériel de 99travl</li>
            <li>Reproduire, dupliquer ou copier le matériel de 99travl</li>
            <li>Redistribuer le contenu de 99travl</li>
        </ul>

        <p>Le présent accord entre en vigueur à la date du présent document.</p>

        <p>Des parties de ce site web offrent aux utilisateurs la possibilité de publier et d'échanger des opinions et
            des informations dans
            certaines zones du site web. 99travl ne filtre pas, n'édite pas, ne publie pas et ne révise pas les
            commentaires avant
            leur présence sur le site web. Les commentaires ne reflètent pas les vues et les opinions de 99travl, de
            ses
            agents et/ou affiliés. Les commentaires reflètent les points de vue et les opinions de la personne qui les
            publie
            et des opinions. Dans la mesure permise par les lois applicables, 99travl ne peut être tenue responsable
            de la
            Commentaires ou pour toute responsabilité, tout dommage ou toute dépense causés et/ou subis du fait de
            l'utilisation
            et/ou la publication et/ou l'apparition des commentaires sur ce site web.</p>

        <p>99travl se réserve le droit de surveiller tous les commentaires et de supprimer tout commentaire qui
            peut être
            considéré comme inapproprié, offensant ou cause une violation des présentes conditions générales.</p>

        <p> Vous garantissez et représentez que:</p>

        <ul>
            <li>Vous êtes autorisé à publier les commentaires sur notre site web et vous disposez de toutes les licences
                et
                y consentent ;
            </li>
            <li>Les commentaires n'empiètent sur aucun droit de propriété intellectuelle, y compris, sans limitation
                les droits d'auteur, les brevets ou les marques de commerce de tout tiers ;
            </li>
            <li>Les commentaires ne contiennent aucun élément diffamatoire, injurieux, offensant, indécent ou autrement
                illégal
                les documents qui constituent une atteinte à la vie privée
            </li>
            <li>Les commentaires ne seront pas utilisés pour solliciter ou promouvoir des affaires ou des coutumes ou
                présenter des
                ou une activité illégale.
            </li>
        </ul>

        <p>Vous accordez par la présente à 99travl une licence non exclusive d'utilisation, de reproduction, d'édition
            et vous autorisez les autres à
            utiliser, reproduire et modifier vos commentaires sous quelque forme, format ou support que ce soit.</p>

        <h2 className='text-primary'>Hyperlien vers notre contenu</h2>

        Les organisations suivantes peuvent créer des liens vers notre site web sans autorisation écrite préalable:
        <p> Les organisations suivantes peuvent créer des liens vers notre site web sans autorisation écrite
            préalable:</p>

        <ul>
            <li>Agences gouvernementales;</li>
            <li>Moteurs de recherche;</li>
            <li>Organismes d'information;</li>
            <li>Les distributeurs d'annuaires en ligne peuvent établir un lien vers notre site web de la même manière
                qu'ils établissent un hyperlien vers
                les sites web d'autres entreprises cotées en bourse ; et
            </li>
            <li>Entreprises accréditées à l'échelle du système, sauf pour la sollicitation d'organismes à but non
                lucratif, les achats de charité
                et les groupes de collecte de fonds à des fins caritatives qui ne peuvent pas établir d'hyperliens avec
                notre site web.
            </li>
        </ul>

        <p>Ces organisations peuvent créer des liens vers notre page d'accueil, vers des publications ou vers d'autres
            informations du site web afin
            aussi longtemps que le lien : (a) ne soit en aucune façon trompeur ; (b) n'implique pas faussement un
            parrainage,
            l'aval ou l'approbation de la partie de liaison et de ses produits et/ou services ; et (c) s'inscrit dans
            le contexte du site de l'auteur du lien.</p>

        <p>Nous pouvons examiner et approuver d'autres demandes de liens émanant des types d'organisations suivants:</p>

        <ul>
            <li> sources d'information courantes sur les consommateurs et/ou les entreprises;</li>
            <li>dot.com sites communautaires;</li>
            <li>associations ou autres groupes représentant des organisations caritatives;</li>
            <li>distributeurs d'annuaires en ligne;</li>
            <li>portails Internet;</li>
            <li>comptabilité, droit et cabinets de conseil ; et</li>
            <li>établissements d'enseignement et associations professionnelles.</li>
        </ul>

        <p>Nous approuverons les demandes de liens de ces organisations si nous en décidons ainsi : (a) le lien ne
            ferait pas
            nous avons une image défavorable de nous-mêmes ou de nos entreprises accréditées ; b) l'organisation n'a
            tout enregistrement négatif chez nous ; c) le bénéfice que nous tirons de la visibilité de l'hyperlien
            compense
            l'absence de 99travl ; et (d) le lien se trouve dans le contexte d'informations générales sur les
            ressources.</p>

        <p>Ces organisations peuvent mettre un lien vers notre page d'accueil, à condition que le lien soit bien visible
            : a) ne soit en aucune façon trompeur ;
            (b) n'implique pas faussement le parrainage, l'appui ou l'approbation de la partie qui établit le lien et de
            ses
            produits ou services ; et (c) s'inscrit dans le contexte du site de la partie qui établit le lien.</p>

        <p>Si vous êtes l'une des organisations énumérées au paragraphe 2 ci-dessus et que vous souhaitez établir un
            lien avec notre
            vous devez nous en informer en envoyant un courrier électronique à 99travl. Veuillez indiquer votre nom,
            votre
            le nom de l'organisation, les coordonnées de contact ainsi que l'URL de votre site, une liste des URL de
            que vous avez l'intention de relier à notre site web, et une liste des URL de notre site vers lesquels vous
            souhaitez
            à lier. Attendez 2 à 3 semaines pour obtenir une réponse.</p>

        <p>Les organismes agréés peuvent établir un hyperlien vers notre site web comme suit</p>

        <ul>
            <li>par l'utilisation de notre raison sociale ; ou</li>
            <li>En utilisant le localisateur de ressources uniformes auquel il est lié ; ou</li>
            <li>En utilisant toute autre description de notre site web liée à ce dernier et qui a un sens dans le cadre
                de la
                le contexte et le format du contenu sur le site de la partie qui propose le lien.
            </li>
        </ul>

        <p>Aucune utilisation du logo de 99travl ou d'autres illustrations ne sera autorisée pour les liens en
            l'absence d'une licence de marque
            accord.</p>

        <h2 className='text-primary'>iFrames</h2>

        <p>Sans autorisation préalable et écrite, vous ne pouvez pas créer de cadres autour de nos pages web qui
            modifier de quelque manière que ce soit la présentation visuelle ou l'apparence de notre site web.</p>

        <h2 className='text-primary'>Content Liability</h2>

        <p>Nous ne pouvons être tenus responsables du contenu qui apparaît sur votre site web. Vous acceptez de protéger
            et nous défendre contre toutes les plaintes qui se présentent sur votre site web. Aucun lien ne doit
            apparaître sur un
            Site web pouvant être interprété comme diffamatoire, obscène ou criminel, ou qui enfreint, autrement
            viole, ou préconise la violation ou toute autre violation des droits d'un tiers.</p>

        <h2 className='text-primary'>Réservation des droits</h2>

        <p>Nous nous réservons le droit de vous demander de supprimer tous les liens ou tout lien particulier vers notre
            site web. Vous
            approuver la suppression immédiate de tous les liens vers notre site web sur demande. Nous nous réservons
            également le droit de
            et il est possible de lier la politique à tout moment. En établissant un lien permanent avec notre
            vous acceptez d'être lié à ces conditions de lien et de les respecter.</p>

        <h2 className='text-primary'>Suppression des liens de notre site web</h2>

        <p>Si vous trouvez un lien sur notre site Web qui est offensant pour une raison quelconque, vous êtes libre
            de contacter et
            nous informer à tout moment. Nous examinerons les demandes de suppression de liens, mais nous ne sommes
            pas obligés de le faire ou
            pour vous répondre directement.</p>

        <p>Nous ne garantissons pas l'exactitude des informations figurant sur ce site web, nous ne garantissons pas
            leur exhaustivité
            ou l'exactitude ; nous ne promettons pas non plus de garantir que le site web reste disponible ou que le
            matériel sur
            le site web est tenu à jour.</p>

        <h2 className='text-primary'>Disclaimer</h2>

        <p>Dans la mesure maximale permise par le droit applicable, nous excluons toute représentation, garantie et
            les conditions relatives à notre site web et à l'utilisation de ce site. Rien dans cette clause de
            non-responsabilité ne:</p>

        <ul>
            <li> limiter ou exclure notre ou votre responsabilité en cas de décès ou de blessure corporelle;</li>
            <li> limiter ou exclure notre ou votre responsabilité en cas de fraude ou de fausse déclaration;</li>
            <li> limiter nos ou vos responsabilités de toute manière non autorisée par la législation applicable ;
                ou
            </li>
            <li>exclure toute responsabilité qui ne peut être exclue en vertu de la législation applicable.</li>
        </ul>

        <p>Les limitations et interdictions de responsabilité énoncées dans cette section et ailleurs dans le
            présent avis de non-responsabilité :
            (a) sont soumises au paragraphe précédent ; et (b) régissent toutes les responsabilités découlant de la
            la clause de non-responsabilité, y compris les responsabilités contractuelles, délictuelles et pour
            manquement à une obligation légale.</p>

        <p>À condition que le site web et les informations et services qu'il contient soient fournis gratuitement,
            nous ne sommes pas responsables des pertes ou dommages de quelque nature que ce soit.</p>
    </>
))
