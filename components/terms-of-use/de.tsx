import React, {memo} from "react";

export const TermsOfUseGerman = memo(() => (
    <>
        <h1 className='text-primary'>
            Bedingungen und Konditionen
        </h1>

        <p>Willkommen bei 99travl!</p>

        <p>
            Diese Seite existiert auch in <a href='/en/resources/terms-of-use'>Englisch</a>.
            Im Konfliktfall ist die englische Fassung maßgebend.
        </p>

        <p>Diese Bedingungen und Konditionen umreißen die Regeln und Vorschriften für die Nutzung der 99travl's
            Website,
            zu finden unter https://99travl.com.</p>

        <p>Durch den Zugriff auf diese Website gehen wir davon aus, dass Sie diese Bedingungen und Konditionen
            akzeptieren. Verwenden Sie nicht weiterhin
            99travl, wenn Sie nicht damit einverstanden sind, alle auf dieser Seite genannten Bedingungen und
            Konditionen zu akzeptieren.</p>

        <p>Für diese Allgemeinen Geschäftsbedingungen, die Datenschutzerklärung und den Haftungsausschluss gilt folgende
            Terminologie
            Bekanntmachung und alle Vereinbarungen: "Kunde", "Sie" und "Ihr" bezieht sich auf Sie, die Person, die sich
            auf dieser Website einloggt
            und in Übereinstimmung mit den Geschäftsbedingungen des Unternehmens. "Das Unternehmen", "Wir selbst",
            "Wir", "Unser" und
            "Wir", bezieht sich auf unser Unternehmen. "Partei", "Parteien" oder "Wir", bezieht sich sowohl auf den
            Kunden als auch auf uns selbst.
            Alle Begriffe beziehen sich auf das Angebot, die Annahme und die Berücksichtigung der Zahlung, die zur
            Durchführung der
            Prozess unserer Unterstützung für den Kunden in der am besten geeigneten Weise zum ausdrücklichen Zweck der
            Befriedigung der Bedürfnisse des Kunden in Bezug auf die Bereitstellung der angegebenen Dienstleistungen der
            Firma, in Übereinstimmung
            mit und vorbehaltlich des geltenden niederländischen Rechts. Jede Verwendung der oben genannten Terminologie
            oder anderer Wörter
            in der Einzahl, Mehrzahl, Groß-/Kleinschreibung und/oder er/sie oder sie als austauschbar angesehen werden
            und
            daher als auf dasselbe bezogen. Unsere Allgemeinen Geschäftsbedingungen wurden mit Hilfe der <a
                href="https://www.privacypolicyonline.com/terms-conditions-generator/">Bedingungen und Konditionen
                Generator</a> und die <a href="https://www.generateprivacypolicy.com">Datenschutzpolitik
                Generator</a>.</p>

        <h2 className='text-primary'>Cookies</h2>

        <p>Wir verwenden die Verwendung von Cookies. Mit dem Zugriff auf 99travl haben Sie der Verwendung von Cookies
            in Übereinstimmung mit dem
            99travl's Datenschutzerklärung.</p>

        <p>Die meisten interaktiven Websites verwenden Cookies, damit wir bei jedem Besuch die Daten des Benutzers
            abrufen können. Cookies
            werden von unserer Website verwendet, um die Funktionalität bestimmter Bereiche zu ermöglichen, um es den
            Menschen leichter zu machen
            Besuch unserer Website. Einige unserer Partner/Werbepartner können ebenfalls Cookies verwenden.</p>

        <h2 className='text-primary'>Lizenz</h2>

        <p>Wenn nicht anders angegeben, besitzen 99travl und/oder seine Lizenzgeber die geistigen Eigentumsrechte für
            alle
            Material über 99travl. Alle Rechte an geistigem Eigentum sind vorbehalten. Sie können darauf zugreifen von
            99travl für Ihren eigenen persönlichen Gebrauch, der den in diesen Geschäftsbedingungen festgelegten
            Einschränkungen unterliegt.</p>

        <p>Dürfen Sie nicht:</p>
        <ul>
            <li>Material von 99travl wiederveröffentlichen</li>
            <li>Material von 99travl verkaufen, mieten oder unterlizenzieren</li>
            <li>Material aus 99travl reproduzieren, duplizieren oder kopieren</li>
            <li>Inhalt von 99travl weiterverbreiten</li>
        </ul>

        <p>Dieses Abkommen beginnt mit dem Datum dieses Abkommens.</p>

        <p>Teile dieser Website bieten den Benutzern die Möglichkeit, Meinungen und Informationen zu veröffentlichen und
            auszutauschen in
            bestimmte Bereiche der Website. 99travl filtert, bearbeitet, veröffentlicht oder überprüft Kommentare
            nicht vor
            ihre Präsenz auf der Website. Die Kommentare spiegeln nicht die Ansichten und Meinungen von 99travl,
            seiner
            Vertreter und/oder Partner. Die Kommentare spiegeln die Ansichten und Meinungen der Person wider, die ihre
            Ansichten veröffentlicht.
            und Meinungen. Soweit nach den geltenden Gesetzen zulässig, ist 99travl nicht haftbar für die
            Kommentare oder für jegliche Haftung, Schäden oder Ausgaben, die verursacht und/oder erlitten wurden als
            Ergebnis der Verwendung von
            und/oder die Veröffentlichung und/oder das Erscheinen der Kommentare auf dieser Website.</p>

        <p>99travl behält sich das Recht vor, alle Kommentare zu überwachen und alle Kommentare zu entfernen, die
            als unangemessen, beleidigend betrachtet werden oder einen Verstoß gegen diese Bedingungen verursachen.</p>

        <p>Sie garantieren und versichern, dass:</p>

        <ul>
            <li>Sie sind berechtigt, die Kommentare auf unserer Website zu veröffentlichen und verfügen über alle
                erforderlichen Lizenzen und
                stimmt dem zu;
            </li>
            <li>Die Kommentare verletzen kein geistiges Eigentumsrecht, einschließlich und ohne Einschränkung
                Urheberrecht, Patent oder Warenzeichen von Dritten;
            </li>
            <li>Die Kommentare enthalten keine diffamierenden, verleumderischen, beleidigenden, anstößigen,
                unanständigen oder anderweitig ungesetzlichen
                Material, das eine Verletzung der Privatsphäre darstellt
            </li>
            <li>Die Kommentare werden nicht verwendet, um Geschäfte oder Gewohnheiten zu bewerben oder zu fördern oder
                kommerzielle
                Aktivitäten oder ungesetzliche Aktivitäten.
            </li>
        </ul>

        <p>Sie gewähren 99travl hiermit eine nicht-exklusive Lizenz zur Nutzung, Reproduktion, Bearbeitung und
            Autorisierung anderer
            Ihre Kommentare in allen Formen, Formaten oder Medien zu verwenden, zu reproduzieren und zu bearbeiten.</p>

        <h2 className='text-primary'>Hyperlinking zu unserem Inhalt</h2>

        <p>Die folgenden Organisationen können ohne vorherige schriftliche Genehmigung auf unsere Website verlinken:</p>

        <ul>
            <li>Regierungsbehörden;</li>
            <li>Suchmaschinen;</li>
            <li>Nachrichtenorganisationen;</li>
            <li>Online-Vertriebshändler können auf unsere Website auf die gleiche Weise verlinken, wie sie einen
                Hyperlink zu
                die Websites anderer börsennotierter Unternehmen; und
            </li>
            <li>Systemweit akkreditierte Unternehmen mit Ausnahme der Werbung für gemeinnützige Organisationen,
                Charity-Shopping
                Einkaufszentren und Wohltätigkeits-Fundraising-Gruppen, die keine Hyperlinks zu unserer Website
                enthalten dürfen.
            </li>
        </ul>

        <p>Diese Organisationen können einen Link zu unserer Homepage, zu Publikationen oder zu anderen
            Website-Informationen setzen, so
            so lange wie der Link: (a) in keiner Weise trügerisch ist; (b) nicht fälschlicherweise ein Sponsoring
            impliziert,
            Befürwortung oder Billigung der verknüpfenden Partei und ihrer Produkte und/oder Dienstleistungen; und (c)
            passt in
            den Kontext der Website der verweisenden Partei.</p>

        <p>Wir können andere Link-Anfragen von den folgenden Arten von Organisationen prüfen und genehmigen:</p>

        <ul>
            <li>bekannte Verbraucher- und/oder Geschäftsinformationsquellen;</li>
            <li>dot.com Gemeinschaftsseiten;</li>
            <li>Vereinigungen oder andere Gruppen, die Wohltätigkeitsorganisationen vertreten;</li>
            <li>Online-Verzeichnis-Verteiler;</li>
            <li>Internet-Portale;</li>
            <li>Buchhaltungs-, Rechts- und Beratungsfirmen; und</li>
            <li>Bildungseinrichtungen und Berufsverbände.</li>
        </ul>

        <p>Wir werden Link-Anfragen dieser Organisationen genehmigen, wenn wir das entscheiden: (a) der Link nicht
            machen würde
            wir uns selbst oder unseren akkreditierten Unternehmen gegenüber ungünstig eingestellt sind; (b) die
            Organisation nicht über
            alle negativen Aufzeichnungen bei uns; (c) der Nutzen, der uns durch die Sichtbarkeit des Hyperlinks
            entsteht, kompensiert
            die Abwesenheit von 99travl; und (d) der Link steht im Zusammenhang mit allgemeinen
            Ressourceninformationen.</p>

        <p>Diese Organisationen können auf unsere Homepage verlinken, solange der Link besteht: (a) in keiner Weise
            irreführend ist;
            (b) nicht fälschlicherweise eine Unterstützung, Befürwortung oder Billigung der verlinkenden Partei und
            ihrer
            Produkte oder Dienstleistungen; und (c) in den Kontext der Website der verweisenden Partei passt.</p>

        <p>Wenn Sie eine der in Absatz 2 oben aufgeführten Organisationen sind und an einem Link zu unseren
            Website, müssen Sie uns informieren, indem Sie eine E-Mail an 99travl schicken. Bitte geben Sie Ihren
            Namen, Ihre
            Name der Organisation, Kontaktinformationen sowie die URL Ihrer Website, eine Liste aller URLs von
            die Sie mit unserer Website verlinken möchten, sowie eine Liste der URLs auf unserer Website, zu denen Sie
            einen Link wünschen
            zu verlinken. Warten Sie 2-3 Wochen auf eine Antwort.</p>

        <p>Zugelassene Organisationen können wie folgt auf unsere Website verlinken:</p>

        <ul>
            <li>unter Verwendung unseres Firmennamens; oder</li>
            <li>unter Verwendung des Uniform Resource Locator, mit dem verknüpft ist; oder</li>
            <li>Durch die Verwendung jeder anderen Beschreibung unserer Website, auf die ein Link verweist, der
                innerhalb der
                Kontext und Format des Inhalts auf der Website der verweisenden Partei.
            </li>
        </ul>

        <p>Die Verwendung des 99travl-Logos oder anderer Kunstwerke für die Verlinkung ist ohne Markenlizenz nicht
            gestattet.
            Zustimmung.</p>

        <h2 className='text-primary'>iFrames</h2>

        <p>Ohne vorherige Genehmigung und schriftliche Erlaubnis dürfen Sie keine Frames um unsere Webseiten erstellen,
            die
            die visuelle Präsentation oder das Erscheinungsbild unserer Website in irgendeiner Weise zu verändern.</p>

        <h2 className='text-primary'>Inhaltshaftung</h2>

        <p>Wir übernehmen keine Verantwortung für Inhalte, die auf Ihrer Website erscheinen. Sie verpflichten sich zum
            Schutz
            und verteidigen Sie uns gegen alle Klagen, die auf Ihrer Website erhoben werden. Kein Link(s) sollte(n) auf
            irgendeiner
            Website, die als verleumderisch, obszön oder kriminell interpretiert werden kann, oder die anderweitig gegen
            die Rechte Dritter verletzt oder die Verletzung oder sonstige Verletzung von Rechten Dritter
            befürwortet.</p>

        <h2 className='text-primary'>Rechtsvorbehalt</h2>

        <p>Wir behalten uns das Recht vor, Sie aufzufordern, alle Links oder einen bestimmten Link zu unserer Website zu
            entfernen. Sie
            stimmen zu, alle Links zu unserer Website auf Anfrage sofort zu entfernen. Wir behalten uns außerdem das
            Recht vor
            diese Bedingungen und ihre Verknüpfungsrichtlinie jederzeit ändern. Durch kontinuierliche Verlinkung mit
            unserer
            Website erklären Sie sich damit einverstanden, an diese Verlinkungsbedingungen gebunden zu sein und sie zu
            befolgen.</p>

        <h2 className='text-primary'>Entfernung von Links von unserer Website</h2>

        <p>Wenn Sie auf unserer Website einen Link finden, der aus irgendeinem Grund anstößig ist, können Sie sich
            jederzeit an
            informieren Sie uns jeden Moment. Wir werden Anfragen zur Entfernung von Links prüfen, sind aber nicht dazu
            verpflichtet, oder
            um Ihnen direkt zu antworten.</p>

        <p>Wir garantieren nicht, dass die Informationen auf dieser Website korrekt sind, wir übernehmen keine Garantie
            für ihre Vollständigkeit.
            oder Genauigkeit; auch versprechen wir nicht sicherzustellen, dass die Website verfügbar bleibt oder dass
            das Material auf
            die Website wird auf dem neuesten Stand gehalten.</p>

        <h2 className='text-primary'>Disclaimer</h2>

        <p>Im größtmöglichen durch das anwendbare Recht zulässigen Umfang schließen wir alle Zusicherungen,
            Gewährleistungen und
            Bedingungen in Bezug auf unsere Website und die Nutzung dieser Website. Nichts in diesem Haftungsausschluss
            wird:</p>

        <ul>
            <li>unsere oder Ihre Haftung für Tod oder Körperverletzung begrenzen oder ausschließen;</li>
            <li>unsere oder Ihre Haftung für Betrug oder betrügerische Falschdarstellung einschränken oder
                ausschließen;
            </li>
            <li>eine unserer oder Ihrer Verpflichtungen in einer Weise zu begrenzen, die nach geltendem Recht nicht
                zulässig ist; oder
            </li>
            <li>eine unserer oder Ihrer Verpflichtungen ausschließen, die nach geltendem Recht nicht ausgeschlossen
                werden dürfen.
            </li>
        </ul>

        <p>Die in diesem Abschnitt und an anderer Stelle in diesem Haftungsausschluss festgelegten
            Haftungsbeschränkungen und -verbote:
            (a) unterliegen dem vorstehenden Absatz; und (b) regeln alle Haftungen, die sich aus dem
            Haftungsausschluss, einschliesslich Haftung aus Vertrag, unerlaubter Handlung und wegen Verletzung
            gesetzlicher Pflichten.</p>

        <p>Solange die Website und die Informationen und Dienstleistungen auf der Website kostenlos zur Verfügung
            gestellt werden,
            wir sind nicht haftbar für Verluste oder Schäden irgendwelcher Art.</p>
    </>
))
