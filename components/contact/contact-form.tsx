import React, {useCallback, useState} from "react";
import {FormikHelpers} from "formik/dist/types";
import {Formik} from "formik";
import {Button, Col, Form, Image, Jumbotron} from "react-bootstrap";
import * as yup from "yup";
import "yup-phone";
import {useTranslation} from "next-i18next";

const schema = yup.object({
    name: yup.string()
        .min(3, 'length.min-name')
        .max(64, 'length.max-name')
        .required('required.name'),
    email: yup.string()
        .email('value.email')
        .required('required.email'),
    phoneNumber: yup.string()
        .phone(undefined, true, 'value.phone-number')
        .min(3, 'length.min-phone-number')
        .max(20, 'length.max-phone-number'),
    subject: yup.string()
        // .required('required.subject')
        .min(3, 'length.min-subject')
        .max(200, 'length.max-subject'),
    message: yup.string()
        // .required('required.message-body')
        .min(8, 'length.min-message-body')
        .max(64, 'length.max-message-body')
});

interface ContactValues {
    name: string;
    email: string;
    phoneNumber: string;
    subject: string;
    message: string;
}

const ErrorField = ({t, message}: any) => message ? (
    <Form.Control.Feedback type="invalid">
        {t('common:validation.' + message)}
    </Form.Control.Feedback>
) : null;

const ContactForm = () => {
    const {t} = useTranslation('contact');

    const [show, setShow] = useState<boolean>(false);
    const [sending, setSending] = useState<boolean>(false);

    const sendAgain = useCallback(() => setShow(false), []);
    const handleShow = useCallback(() => setShow(true), []);

    const submitForm = async (values: ContactValues, {resetForm}: FormikHelpers<ContactValues>) => {
        setSending(true);
        try {
            // TODO

            await fetch('/api/contact/send', {
                body: JSON.stringify(values),
                method: 'post',
                credentials: 'same-origin',
                cache: 'no-cache',
            });

            resetForm();
            handleShow();
        } catch (e) {
            console.error(e);
        }

        setSending(false);
    }

    if (show) {
        return (
            <Jumbotron className='mt-2 mt-md-3 w-100'>
                <Image
                    alt='check'
                    src='/stores/check.svg'
                    className='w-25 mb-2 mb-md-3 mb-lg-5 d-none d-md-inline'
                    roundedCircle
                />

                <h1 className='h1 font-weight-light text-primary'>
                    {t('contact-form.sent.title')}
                </h1>

                <p>
                    {t('contact-form.sent.message')}
                </p>

                <p>
                    <Button onClick={sendAgain}>
                        {t('contact-form.sent.again')}
                    </Button>
                </p>
            </Jumbotron>
        )
    }

    return (
        <Formik<ContactValues>
            validationSchema={schema}
            onSubmit={submitForm}
            initialValues={{
                name: '',
                email: '',
                phoneNumber: '',
                subject: '',
                message: ''
            }}
        >
            {({
                  handleSubmit,
                  handleChange,
                  handleBlur,
                  values,
                  touched,
                  isValid,
                  errors,
              }) => (
                <Form noValidate onSubmit={handleSubmit} className='text-left'>
                    <Form.Group controlId="formGridName">
                        <Form.Label lg={2}>{t('common:label.name')}</Form.Label>
                        <Form.Control
                            size="lg"
                            placeholder="Talla Momo"
                            name='name'
                            disabled={sending}
                            value={values.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isValid={touched.name && !errors.name}
                            isInvalid={touched.name && !!errors.name}
                        />
                        <ErrorField message={errors.name}/>
                    </Form.Group>

                    <Form.Row>
                        <Form.Group as={Col} xs="12" md="6" controlId="formGridEmail">
                            <Form.Label lg={2}>{t('common:label.email')}</Form.Label>
                            <Form.Control
                                size="lg"
                                type="email"
                                placeholder="my.email@99travl.com"
                                name='email'
                                disabled={sending}
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.email && !errors.email}
                                isInvalid={touched.email && !!errors.email}
                            />
                            <ErrorField message={errors.email}/>
                        </Form.Group>

                        <Form.Group as={Col} xs="12" md="6" controlId="formGridPhoneNumber">
                            <Form.Label lg={2}>{t('common:label.phone-number')}</Form.Label>
                            <Form.Control
                                size="lg"
                                type="text"
                                placeholder="e.g +123 45 67 890"
                                name='phoneNumber'
                                disabled={sending}
                                value={values.phoneNumber}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.phoneNumber && !errors.phoneNumber}
                                isInvalid={touched.phoneNumber && !!errors.phoneNumber}
                            />
                            <ErrorField message={errors.phoneNumber}/>
                        </Form.Group>
                    </Form.Row>

                    <Form.Group controlId="formGridSubject">
                        <Form.Label lg={2}>{t('common:label.subject')}</Form.Label>
                        <Form.Control
                            size="lg"
                            placeholder={t('contact-form.placeholder.subject')}
                            name='subject'
                            disabled={sending}
                            value={values.subject}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isValid={touched.subject && !errors.subject}
                            isInvalid={touched.subject && !!errors.subject}
                        />
                        <ErrorField message={errors.subject}/>
                    </Form.Group>

                    <Form.Group controlId="formGridMessage">
                        <Form.Label lg={2}>
                            {t('common:label.message')}
                        </Form.Label>
                        <Form.Control
                            as='textarea'
                            rows={4}
                            size="lg"
                            placeholder={t('contact-form.placeholder.message')}
                            name='message'
                            disabled={sending}
                            value={values.message}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isValid={touched.message && !errors.message}
                            isInvalid={touched.message && !!errors.message}
                        />
                        <ErrorField message={errors.message}/>
                    </Form.Group>

                    <Button size='lg'
                            block
                            disabled={sending}
                            className='mt-2 mt-sm-4 mt-lg-5'
                            variant="outline-primary" type="submit">
                        {t('common:submit')}
                    </Button>
                </Form>
            )}
        </Formik>
    );
}

ContactForm.getInitialProps = async () => ({
    namespacesRequired: ['contact', 'common'],
});

export default ContactForm;
