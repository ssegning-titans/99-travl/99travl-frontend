import {Col, Container, Form, Image, Row} from "react-bootstrap";
import {useTranslation, withTranslation} from "next-i18next";
import Link from 'next/link';
import {useRouter} from "next/router";
import {ChangeEvent, useCallback} from "react";
import {languageLabels} from "@common/languages";



export function AppFooter() {
    const {t: ft} = useTranslation('footer');
    const {t: wt} = useTranslation('welcome');
    const router = useRouter();

    const year = new Date().getFullYear();

    const handleLanguageChange = useCallback(({target: {value: lang}}: ChangeEvent<HTMLSelectElement>) => {
        if (!router) return;
        const route = router.route;
        router.push(route, route, {locale: lang});
    }, [router?.locale]);

    return (
        <footer className='footer pb-3'>
            <Container>
                <hr className='py-2 py-md-3 mt-md-5'/>

                <Row className='justify-content-md-between'>
                    <Col md={4} className='pb-3 pb-md-0'>
                        <Link href='/'>
                            <a className='main-logo'>
                                <Row>
                                    <Col className='text-center text-md-left'>
                                        <img
                                            src="/logo.png"
                                            height="42"
                                            width='109.16'
                                            className="d-inline-block align-top"
                                            alt="99Travl logo"
                                        />
                                    </Col>
                                </Row>
                            </a>
                        </Link>

                        <Row className='d-none d-md-block'>
                            <Col>
                                <h3 className='text-muted'>{wt('catchPhrase')}</h3>
                                <p>{wt('catchPhrase')}</p>
                            </Col>
                        </Row>
                    </Col>

                    <Col md className='text-center text-md-left pb-3 pb-md-0'>
                        <h6 className='font-weight-bold'>{ft('company.title')}</h6>

                        <ul className='list-unstyled text-small'>
                            <li>
                                <Link href='/company/about'>{ft('company.about')}</Link>
                            </li>
                            <li>
                                <Link href='/company/partners'>{ft('company.partners')}</Link>
                            </li>
                            <li>
                                <Link href='/company/contact'>{ft('company.contact')}</Link>
                            </li>
                        </ul>
                    </Col>

                    <Col md className='text-center text-md-left pb-3 pb-md-0'>
                        <h6 className='font-weight-bold'>{ft('resources.title')}</h6>

                        <ul className='list-unstyled text-small'>
                            <li>
                                <Link href='/resources/terms-of-use'>{ft('resources.terms-of-use')}</Link>
                            </li>
                            <li>
                                <Link href='/resources/privacy-policy'>{ft('resources.privacy-policy')}</Link>
                            </li>
                            <li>
                                <Link href='/resources/support'>{ft('resources.support')}</Link>
                            </li>
                        </ul>
                    </Col>

                    <Col md className='text-center text-md-left pb-3 pb-md-0'>
                        <h6 className='font-weight-bold'>{ft('more.title')}</h6>

                        <ul className='list-unstyled text-small'>
                            <li>
                                <a href='https://let-talk.com/' target='_blank' rel="noopener">Let-talk</a>
                            </li>
                            <li>
                                <a rel='noopener' href='https://codingmotion.com' target='_blank'>Codingmotion</a>
                            </li>
                            <li>
                                <Link href='/company/philosophy'>{ft('more.philosophy')}</Link>
                            </li>
                        </ul>
                    </Col>

                    <Col md={2} className='text-center text-md-left pb-3 pb-md-0'>
                        <h6 className='font-weight-bold d-none d-md-block'>Download Apps</h6>
                        <a className="store-img"
                           rel='noopener'
                           href='https://play.google.com/store/apps/details?id=com.ssegning.ntravl'
                           target='_blank'>
                            <Image alt='play store image' src="/stores/google-play-badge.png" className='my-1'/>
                        </a>
                        <a className="store-img"
                           href='/'
                           rel='noopener'
                           target='_blank'>
                            <Image alt='app store image' src="/stores/appstore.png" className='my-1'/>
                        </a>
                    </Col>
                </Row>
                <Row className='mt-3 mt-md-5 justify-content-md-between align-items-md-end'>
                    <Col md={3} className='text-center text-md-left'>
                        <Form>
                            <Form.Group>
                                <Form.Label>{ft('languageLabel')}</Form.Label>
                                <Form.Control as="select" value={router.locale} onChange={handleLanguageChange} custom>
                                    {languageLabels.map((language, index) => (
                                        <option value={language.value} key={language.value}>{language.label}</option>
                                    ))}
                                </Form.Control>
                            </Form.Group>
                        </Form>
                    </Col>

                    <Col md={3} className='text-center text-md-right'>
                        <p>© {year} - 99Travl</p>
                    </Col>
                </Row>

                <hr className='py-2 mt-5'/>
            </Container>
            <div className='text-center py-3'>
                <a href='/' target='_blank' rel='noopener' className='marketing-logo'>
                    <img width="38"
                         height="38"
                         src='/stores/f_logo_RGB-Blue_58.png'
                         alt="Facebook logo"
                    />
                </a>
                <a href='/' target='_blank' rel='noopener' className='marketing-logo'>
                    <img width="38"
                         height="38"
                         src='/stores/insta.png'
                         alt="Instagramm logo"
                    />
                </a>
                <a href='/' target='_blank' rel='noopener' className='marketing-logo'>
                    <img width="38"
                         height="38"
                         src='/stores/whatsapp.png'
                         alt="Whastapp logo"
                    />
                </a>
                <a href='/' target='_blank' rel='noopener' className='marketing-logo'>
                    <img width="38"
                         height="38"
                         src='/stores/Twitter_Social_Icon_Circle_Color.png'
                         alt="Twitter logo"
                    />
                </a>
            </div>
        </footer>
    );
}
