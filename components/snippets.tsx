import React, {memo} from "react";
import {Container} from "react-bootstrap";

interface ContProps {
    children: JSX.Element | JSX.Element[];
    id?: string;
    center?: boolean;
    gradBg?: boolean;
    src?: string;
    className?: string;
}

export const Cont = memo(({children, id, center, gradBg, src, className}: ContProps) => (
    <div
        className={`py-5` + (gradBg ? ' bg-primary-gradient text-white' : '') + (center ? ' text-md-center' : '') + (className ? ' ' + className : '')}
        style={src && {
            backgroundImage: `url(${src})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPositionX: 'center',
            backgroundPositionY: 'center'
        }}>
        <Container id={id} className='my-1 my-md-3 my-lg-5'>
            {children}
        </Container>
    </div>
));