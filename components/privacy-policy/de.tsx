import {memo} from "react";

export const PrivacyPolicyGerman = memo(() => (
    <>
        <h1 className='text-primary'>Datenschutzpolitik von 99travl</h1>

        <p>
            Diese Politik existiert auch in <a href='/en/resources/privacy-policy'>Englisch</a>.
            Im Konfliktfall ist die englische Fassung maßgebend.
        </p>

        <p>99travl betreibt die Website https://99travl.com, die den SERVICE anbietet.</p>

        <p>Diese Seite dient dazu, Website-Besucher über unsere Richtlinien bezüglich der Sammlung, Nutzung und
            Offenlegung von persönlichen Informationen, wenn jemand unseren Service, die 99travl.com, nutzen möchte
            Website.</p>

        <p>Wenn Sie sich für die Nutzung unseres Dienstes entscheiden, stimmen Sie der Erfassung und Nutzung von
            Informationen in Bezug auf
            mit dieser Politik. Die persönlichen Daten, die wir sammeln, werden für die Bereitstellung und Verbesserung
            der
            Dienst. Wir werden Ihre Informationen nicht verwenden oder an Dritte weitergeben, außer wie in dieser
            Datenschutzerklärung beschrieben.
            Politik. Unsere Datenschutzrichtlinie wurde mit Hilfe der <a
                href="https://www.privacypolicytemplate.net">Vorlage für Datenschutzrichtlinien</a> und die <a
                href="https://www.generateprivacypolicy.com">Datenschutzpolitik-Generator</a>.</p>

        <p>Die in dieser Datenschutzrichtlinie verwendeten Begriffe haben die gleiche Bedeutung wie in unseren
            Allgemeinen Geschäftsbedingungen, nämlich
            zugänglich unter https://99travl.com, sofern in dieser Datenschutzerklärung nicht anders definiert.</p>

        <h2 className='text-primary'>Informationssammlung und -nutzung</h2>

        <p>Für eine bessere Erfahrung bei der Nutzung unseres Dienstes müssen Sie uns möglicherweise bestimmte Angaben
            machen
            persönlich identifizierbare Informationen, einschließlich, aber nicht beschränkt auf Ihren Namen, Ihre
            Telefonnummer und
            Postanschrift. Die Informationen, die wir sammeln, werden verwendet, um Sie zu kontaktieren oder zu
            identifizieren.</p>

        <h2 className='text-primary'>Daten protokollieren</h2>

        <p>Wir möchten Sie darüber informieren, dass wir, wann immer Sie unseren Service besuchen, Informationen
            sammeln, die Ihr Browser
            an uns sendet, das Logdaten genannt wird. Diese Protokolldaten können Informationen wie z.B. die
            Internet-Protokoll-Adresse ("IP-Adresse"), Browser-Version, die von Ihnen besuchten Seiten unseres Dienstes,
            die Uhrzeit und
            Datum Ihres Besuchs, die auf diesen Seiten verbrachte Zeit und andere Statistiken.</p>

        <h2 className='text-primary'>Cookies</h2>

        <p>Cookies sind Dateien mit einer kleinen Datenmenge, die üblicherweise als anonyme, eindeutige Kennung
            verwendet wird.
            Diese werden von der Website, die Sie besuchen, an Ihren Browser gesendet und auf der Festplatte Ihres
            Computers gespeichert.
            Festplatte.</p>

        <p>Unsere Website verwendet diese "Cookies", um Informationen zu sammeln und unseren Service zu verbessern. Sie
            haben die
            die Möglichkeit, diese Cookies entweder zu akzeptieren oder abzulehnen und zu wissen, wann ein Cookie an
            Ihre
            Computer. Wenn Sie sich entscheiden, unsere Cookies abzulehnen, kann es sein, dass Sie einige Teile unserer
            Website nicht nutzen können.
            Dienst.</p>

        <p>Für weitere allgemeine Informationen über Cookies lesen Sie bitte <a
            href="https://www.cookieconsent.com/what-are-cookies/">"Was sind Cookies"</a>.</p>

        <h2 className='text-primary'>Dienstanbieter</h2>

        <p>Wir können aus folgenden Gründen Drittfirmen und Einzelpersonen beschäftigen:</p>

        <ul>
            <li>Um unseren Dienst zu erleichtern;</li>
            <li>Um den Dienst in unserem Namen zu erbringen;</li>
            <li>Dienstleistungsbezogene Dienstleistungen zu erbringen; oder</li>
            <li>Um uns bei der Analyse der Nutzung unseres Dienstes zu unterstützen.</li>
        </ul>

        <p>Wir möchten unsere Service-Benutzer darüber informieren, dass diese Dritten Zugang zu Ihren persönlichen
            Informationen. Der Grund dafür ist, die ihnen übertragenen Aufgaben in unserem Namen zu erfüllen. Sie sind
            jedoch
            verpflichtet, die Informationen nicht offenzulegen oder für andere Zwecke zu verwenden.</p>

        <h2 className='text-primary'>Sicherheit</h2>

        <p>Wir schätzen Ihr Vertrauen, dass Sie uns Ihre persönlichen Daten zur Verfügung stellen, daher sind wir
            bestrebt
            kommerziell akzeptable Mittel, um sie zu schützen. Aber denken Sie daran, dass keine Methode der Übertragung
            über die
            Internet oder die Methode der elektronischen Speicherung ist 100% sicher und zuverlässig, und wir können
            nicht garantieren, dass
            absolute Sicherheit.</p>

        <h2 className='text-primary'>Links zu anderen Sites</h2>

        <p>Unser Service kann Links zu anderen Websites enthalten. Wenn Sie auf einen Link eines Dritten klicken, werden
            Sie
            die an diese Website gerichtet sind. Beachten Sie, dass diese externen Sites nicht von uns betrieben werden.
            Daher möchten wir dringend
            raten wir Ihnen, die Datenschutzrichtlinien dieser Websites zu lesen. Wir haben keine Kontrolle über diese
            Websites und nehmen keine
            die Verantwortung für den Inhalt, die Datenschutzrichtlinien oder die Praktiken von Websites Dritter oder
            Dienstleistungen.</p>

        <p>Privatsphäre der Kinder</p>

        <p>Unsere Dienstleistungen richten sich nicht an Personen unter 13 Jahren. Wir sammeln nicht wissentlich
            persönliche
            identifizierbare Informationen von Kindern unter 13 Jahren. Für den Fall, dass wir feststellen, dass ein
            Kind unter 13
            uns persönliche Informationen zur Verfügung gestellt haben, löschen wir diese umgehend von unseren Servern.
            Wenn Sie ein
            Elternteil oder Erziehungsberechtigter und Sie sind sich bewusst, dass Ihr Kind uns persönliche Daten zur
            Verfügung gestellt hat,
            nehmen Sie bitte Kontakt mit uns auf, damit wir die notwendigen Maßnahmen ergreifen können.</p>

        <h2 className='text-primary'>Änderungen an dieser Datenschutzerklärung</h2>

        <p>Wir können unsere Datenschutzerklärung von Zeit zu Zeit aktualisieren. Wir empfehlen Ihnen daher, diese Seite
            zu überprüfen.
            periodisch für alle Änderungen. Wir werden Sie über alle Änderungen informieren, indem wir die neue
            Datenschutzrichtlinie unter
            diese Seite. Diese Änderungen treten sofort nach ihrer Veröffentlichung auf dieser Seite in Kraft.</p>

        <h2 className='text-primary'>Kontakt uns</h2>

        <p>Wenn Sie Fragen oder Anregungen zu unserer Datenschutzpolitik haben, zögern Sie nicht, uns zu
            kontaktieren.</p>
    </>
));
