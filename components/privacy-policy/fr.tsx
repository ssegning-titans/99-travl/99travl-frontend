import {memo} from "react";

export const PrivacyPolicyFrench = memo(() => (
    <>
        <h1 className='text-primary'>Politique de protection de la vie privée de 99travl</h1>

        <p>
            Cette page existe aussi en <a href='/en/resources/privacy-policy'>anglais</a>.
            En cas de conflits, la version anglaise prévaut.
        </p>

        <p>99travl gère le site web https://99travl.com, qui fournit le SERVICE.</p>

        <p>Cette page est utilisée pour informer les visiteurs du site web sur nos politiques en matière de collecte,
            d'utilisation et de
            la divulgation d'informations personnelles si quelqu'un décidait d'utiliser notre service, le 99travl.com
            site web.</p>

        <p>Si vous choisissez d'utiliser notre service, vous acceptez la collecte et l'utilisation d'informations en
            relation
            avec cette politique. Les informations personnelles que nous recueillons sont utilisées pour fournir et
            améliorer la
            Service. Nous n'utiliserons ni ne partagerons vos informations avec quiconque, sauf dans les conditions
            décrites dans le présent document.
            Politique. Notre politique de confidentialité a été créée à l'aide de la <a
                href="https://www.privacypolicytemplate.net">Modèle de politique de protection de la vie privée</a> et
            le <a
                href="https://www.generateprivacypolicy.com">Générateur de politiques de protection de la vie privée</a>.
        </p>

        <p>Les termes utilisés dans la présente politique de confidentialité ont la même signification que dans nos
            conditions générales, à savoir
            accessible à l'adresse suivante : https://99travl.com, sauf disposition contraire de la présente politique
            de confidentialité.</p>

        <h2 className='text-primary'>Collecte et utilisation des informations</h2>

        <p>Pour une meilleure expérience lors de l'utilisation de notre service, nous pouvons vous demander de nous
            fournir certaines
            des informations permettant de vous identifier, y compris, mais sans s'y limiter, votre nom, votre numéro de
            téléphone et
            l'adresse postale. Les informations que nous recueillons seront utilisées pour vous contacter ou vous
            identifier.</p>

        <h2 className='text-primary'>Log Data</h2>

        <p>Nous tenons à vous informer que chaque fois que vous visitez notre service, nous recueillons des
            informations que votre navigateur
            nous envoie ce qu'on appelle des données de journal. Ces données peuvent comprendre des informations
            telles que les données de votre ordinateur
            l'adresse IP, la version du navigateur, les pages de notre service que vous visitez, l'heure et la date
            de votre visite
            la date de votre visite, le temps passé sur ces pages, et d'autres statistiques.</p>

        <h2 className='text-primary'>Cookies</h2>

        <p>Les cookies sont des fichiers contenant une petite quantité de données et qui sont généralement utilisés
            avec un identifiant unique anonyme.
            Ils sont envoyés à votre navigateur depuis le site web que vous visitez et sont stockés sur le disque
            dur de votre ordinateur
            disque dur.</p>

        <p>Notre site web utilise ces "cookies" pour collecter des informations et améliorer notre service. Vous
            avez le
            d'accepter ou de refuser ces cookies, et de savoir quand un cookie est envoyé à votre
            ordinateur. Si vous choisissez de refuser nos cookies, il se peut que vous ne puissiez pas utiliser
            certaines parties de notre
            Service.</p>

        <p>Pour des informations plus générales sur les cookies, veuillez lire <a
            href="https://www.cookieconsent.com/what-are-cookies/">"Que sont les cookies"</a>.</p>

        <h2 className='text-primary'>Fournisseurs de services</h2>

        <p>Nous pouvons employer des entreprises et des personnes tierces pour les raisons suivantes:</p>

        <ul>
            <li>Pour faciliter notre service;</li>
            <li> Fournir le service en notre nom;</li>
            <li>Pour fournir des services liés aux services ; ou</li>
            <li>Pour nous aider à analyser la manière dont notre service est utilisé.</li>
        </ul>

        <p>Nous souhaitons informer les utilisateurs de notre service que ces tiers ont accès à votre dossier
            personnel
            Information. La raison est d'accomplir les tâches qui leur sont assignées en notre nom. Cependant, ils
            sont
            l'obligation de ne pas divulguer ou utiliser les informations à d'autres fins.</p>

        <h2 className='text-primary'>Sécurité</h2>

        <p>Nous apprécions la confiance que vous nous accordez en nous fournissant vos informations personnelles,
            c'est pourquoi nous nous efforçons d'utiliser
            des moyens commercialement acceptables de la protéger. Mais rappelez-vous qu'aucune méthode de
            transmission sur le
            Internet, ou méthode de stockage électronique, est sûr et fiable à 100 %, et nous ne pouvons pas
            garantir son
            sécurité absolue.</p>

        <h2 className='text-primary'>Liens vers d'autres sites</h2>

        <p>Notre service peut contenir des liens vers d'autres sites. Si vous cliquez sur un lien tiers, vous serez
            dirigé vers ce site. Notez que ces sites externes ne sont pas exploités par nous. Par conséquent, nous
            nous engageons fermement à
            vous conseille de consulter la politique de confidentialité de ces sites web. Nous n'avons aucun
            contrôle sur, et ne supposons pas
            la responsabilité du contenu, des politiques de confidentialité ou des pratiques de tout site tiers ou
            services.</p>

        <p>Vie privée des enfants</p>

        <p>Nos services ne s'adressent pas aux personnes de moins de 13 ans. Nous ne collectons pas sciemment des
            données personnelles
            les informations identifiables des enfants de moins de 13 ans. Dans le cas où nous découvrons qu'un
            enfant de moins de 13 ans a
            nous a fourni des informations personnelles, nous les supprimons immédiatement de nos serveurs. Si vous
            êtes un
            un parent ou un tuteur et vous savez que votre enfant nous a fourni des informations personnelles,
            veuillez nous contacter afin que nous puissions prendre les mesures nécessaires.</p>

        <h2 className='text-primary'>Modifications apportées à cette politique de confidentialité</h2>

        <p>Nous pouvons mettre à jour notre politique de confidentialité de temps en temps. Ainsi, nous vous
            conseillons de consulter cette page
            périodiquement pour tout changement. Nous vous informerons de tout changement en publiant la
            nouvelle politique de confidentialité sur
            cette page. Ces changements entrent en vigueur immédiatement, après avoir été publiés sur cette
            page.</p>

        <h2 className='text-primary'>Contactez-nous</h2>

        <p>Si vous avez des questions ou des suggestions concernant notre politique de confidentialité,
            n'hésitez pas à nous contacter.</p>
    </>
));
