module.exports = {
    i18n: {
        defaultLocale: 'en',
        locales: ['en', 'de', 'ar', 'fr'],
        // Complete with these: https://gist.github.com/jacobbubu/1836273
        domains: [
            {
                domain: '99travl.com',
                defaultLocale: 'en',
                locales: ['en-CM', 'en-US'],
            },
            {
                domain: 'fr.99travl.com',
                defaultLocale: 'fr',
                locales: ['fr-CM', 'fr-FR'],
            },
            {
                domain: 'ar.99travl.com',
                defaultLocale: 'ar',
                locales: ['ar-EG', 'ar-YE', 'ar-SA', 'ar-SD', 'ar-LY', 'ar-MA', 'ar-OM', 'ar-TN', 'ar-JO'],
            },
            {
                domain: 'de.99travl.com',
                defaultLocale: 'de',
                locales: ['de-DE'],
            },
        ],
        localeDetection: false,
    },
    react: {
        useSuspense: false
    }
}
