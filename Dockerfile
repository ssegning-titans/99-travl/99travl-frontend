# Build
FROM node:14-alpine as BUILD_IMAGE

WORKDIR /build

COPY ./package.json .

RUN yarn --frozen-lockfile

COPY . .

RUN yarn build

# Prod image
FROM node:14-alpine

ENV NODE_ENV=production

WORKDIR /app

COPY --from=BUILD_IMAGE /build/package.json ./
COPY --from=BUILD_IMAGE /build/.next ./.next
COPY --from=BUILD_IMAGE /build/public ./public
COPY ./next.config.js ./next.config.js
COPY ./next-i18next.config.js ./next-i18next.config.js

RUN yarn add next sharp

EXPOSE 3000

CMD yarn start
